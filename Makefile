all: login build push
.PHONY: all

login:
	aws ecr get-login --no-include-email --region ap-northeast-1 --profile tsuru | bash
.PHONY: login

build:
	docker build -t wifi-algorithm .
.PHONY: build

push:
	docker tag wifi-algorithm:latest 652062820328.dkr.ecr.ap-northeast-1.amazonaws.com/wifi-algorithm:latest
	docker push 652062820328.dkr.ecr.ap-northeast-1.amazonaws.com/wifi-algorithm:latest
.PHONY: push
