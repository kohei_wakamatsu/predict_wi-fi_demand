# WiFi Demand Algorithm

## Run

```
# ./output/<<今日の日付>>.csv
$ python main.py -i departure_all.csv -C '00 アメリカ'

# Specify the output dir and the start date
$ python main.py -i departure_all.csv -C '00 アメリカ' -o './output/test.csv' -s '2013-04-01'

# Specify the output dir and the start date and the end date
$ python main.py -i departure_all.csv -C '00 アメリカ' -o './output/test.csv' -s '2013-04-01' -e '2017-12-31'
```

## Docker

```
$ docker build -t wifi-algorithm .
$ docker run -it wifi-algorithm bash
```