FROM python:3.6.5-alpine
MAINTAINER Kenji Nomura <kenji.nomura.0203@gmail.com>

RUN apk add --no-cache --virtual .build-deps \
      build-base python-dev openblas-dev freetype-dev libpng pkgconfig gfortran \
      && ln -s /usr/include/locale.h /usr/include/xlocale.h \
      && pip3 install --no-cache-dir --disable-pip-version-check \
      numpy==1.14.3 \
      && pip3 install --no-cache-dir --disable-pip-version-check \
      pandas==0.23.0 \
      scipy==1.1.0 \
      matplotlib==2.2.2 \
      scikit-learn==0.19.1 \
      && apk del .build-deps

RUN apk add --no-cache \
      g++ \
      bash \
      ca-certificates openblas \
      tk tcl \
      linux-headers less docker

WORKDIR /srv

COPY ./ /srv
