
# coding: utf-8

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import RANSACRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split, KFold

import pickle
from copy import deepcopy
from pprint import pprint
import argparse

from data_utils import *
from predict_utils import *


def arg_parse():
    parser = argparse.ArgumentParser()
    parser = argparse.ArgumentParser(
                        prog="main.py", 
                        usage="main.py -i input_file -C contry_string"
                              "[-c country-id][-o --outputpath path]"
                              "[-s --start_date DATE(yyyy-mm-dd)] [-e --end_date DATE(yyyy-mm-dd)]"
                              "[-p --prediction_period value] [-v --view]"
                              "[--n_times n_times] [--n_folds n_folds]", 
                        description="Predict wi-fi demand by input csv file.", 
                        add_help = True 
                        )

    parser.add_argument("-i", "--inputfile", 
                        help = "Input file name.",
                        type = str,
                        required = True)

    parser.add_argument("-c", "--country_id", 
                        help = "Contry id. Please refer to another the correspondence table.",
                        type = int,
                        default = -1)
    
    parser.add_argument("-C", "--country_string", 
                        help = "Contry string. i.e. '00 アメリカ' \
                                If you select all country, please enter 'all'.",
                        type = str,
                        required = True)
    
    parser.add_argument("-o", "--outputpath",
                        help = "Output file name or path.",
                        type = str,
                        default = "output/"+str(pd.datetime.today().strftime("%Y-%m-%d"))+".csv")
    
    parser.add_argument("-s", "--start_date",
                        help = "Beginning of lerning date.",
                        type = str,
                        default = '2013-01-01')
    
    parser.add_argument("-e", "--end_date",
                        help = "End of lerning date.",
                        type = str,
                        default = pd.datetime.today())
    
    parser.add_argument("-p", "--prediction_period",
                        help = "Prediction period",
                        type = int,
                        default = 90)
    
    parser.add_argument("-P", "--prediction_start_date",
                        help = "Prediction start date",
                        type = str,
                        default = pd.datetime.today())

    parser.add_argument("-v", "--view", 
                        help = "If you turn this on, display the graph of predict.",
                        action = 'store_true')

    parser.add_argument("--n_times",
                        help = "Number of generic learning times to predict bias.",
                        type = int,
                        default = 5)

    parser.add_argument("--n_folds",
                        help = "Number of data divisions in kfolds.",
                        type = int,
                        default = 5)
    
    parser.add_argument("-g","--gs",
                        help = "Option of Grid Search to for prediction.",
                        action = 'store_true')
    
    args = parser.parse_args()
    return args


def main():
    args = arg_parse()
    
    print("Start to read data.")
    df_train = read_departure_data(CSV_PATH=args.inputfile, country_string=args.country_string, country_id=args.country_id)
    df_train = add_holiday_information(df_train)
    df_train = extract_by_time(df_train, START_DATE=args.start_date, END_DATE=args.end_date)
    print("Reading data was completed."); print()
    print("Create new dataframe.")
    
    pred_start_date = to_datetime(args.prediction_start_date).strftime('%Y-%m-%d')
    pred_end_date   = (pd.datetime.today()+pd.Timedelta(days=args.prediction_period)).strftime('%Y-%m-%d')
    df_test = create_dataframe(START_DATE=pred_start_date, END_DATE=pred_end_date)
    df_test = add_holiday_information(df_test)
    print("Creating new data was completed."); print()
    
    print("Start to predict demand.")
    ps = PeriodScore().fit(df_train)
    df_train = ps.transform(df_train)
    df_test  = ps.transform(df_test)
    
    dp = DemandPredictor(n_folds=args.n_folds, n_times=args.n_times, view=args.view).fit(df_train)
    
    df_result = pd.DataFrame()
    df_result['index'] = [i for i in range(len(df_test))]
    df_result.set_index('index', inplace=True)
    df_result['date'] = df_test['date']
    df_result['predict'] = dp.predict(df_test)
    df_result.to_csv(args.outputpath)
    
    print();print("Complete!")


if __name__=='__main__':
    main()

