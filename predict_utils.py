
# coding: utf-8

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from collections import defaultdict
from copy import deepcopy
from sklearn.preprocessing import MinMaxScaler, PolynomialFeatures
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.neural_network import MLPRegressor
from sklearn.model_selection import train_test_split, KFold


def visualize_graph(x, y, pred=None, sort=False, figsize=(10, 5)):
    fig = plt.figure(figsize=figsize)
    x_seq = [x for x in range(len(x))]
    if(sort):
        plt.plot(x_seq, np.sort(y), color='blue', label='true')
        if (pred is not None):
            plt.plot(x_seq, np.sort(pred), color='red', label='predict')
    else:
        plt.plot(x_seq, y, color='blue', label='true')
        if (pred is not None):
            plt.plot(x_seq, pred, color='red', label='predict')
    plt.legend()
    plt.show()


def calc_accuracy(actual, predict):
    diff = abs(actual - predict)
    div = np.divide(diff, actual, out=np.zeros_like(diff), where=actual!=0)
    result = 100 - div*100
    result = result[np.where(result > 0)]
    return result.mean()


class DemandPredictor():
    
    def __init__(self, n_times=5, n_folds=5, view=False, gs=False):
        self.n_times = n_times
        self.n_folds = n_folds
        self.view = view
        self.gs = gs
    
    
    def _fit_linear(self, X, y):
        self.reg = LinearRegression().fit(X, y)
    
    
    def _fit_bias(self, X, y, rfr=False):
        if(rfr):
            origin_model = RandomForestRegressor(n_estimators=500)
        else:
            origin_model = MLPRegressor(hidden_layer_sizes=(128, 256, 128, ))
        
        self.models = defaultdict(dict)
        self.kfolds = {}
        
        for i in range(self.n_times):
            print("Section : {}".format(i))
            print("Classifier : ", end="")
            kf = KFold(n_splits=self.n_folds, shuffle=True)
            self.kfolds[i] = kf

            for k, (train_index, _) in enumerate(kf.split(X)):
                print("{0:2d}".format(k), end="")
                rfr = deepcopy(origin_model)
                X_train = X[train_index]
                y_train = y[train_index]

                rfr.fit(X_train, y_train)
                self.models[i][k] = rfr

            print()
        
            
    def _predict_bias(self, X, test=False):
        predict_arr = np.zeros((self.n_times, len(X)))
        
        if(test):
            for i in range(self.n_times):
                for j in range(self.n_folds):
                    predict_arr[i] = predict_arr[i] + self.models[i][j].predict(X)
                predict_arr[i] = predict_arr[i] / self.n_folds
        else:
            for i, kf in enumerate(self.kfolds.values()):
                for j, (_, test_index) in enumerate(kf.split(X)):
                    predict_arr[i][test_index] =                         predict_arr[i][test_index] + self.models[i][j].predict(X[test_index])

        predict_arr_result = predict_arr.mean(axis=0)
        return predict_arr_result
        

        
    def _get_explain_data(self, df, linear, test=False):
        def user_poly(x):
            new_features = np.zeros((x.shape[1]-1, x.shape[0]))
            for i in range(x.shape[1]-1):
                new_features[i] = x.transpose(1, 0)[0] * x.transpose(1, 0)[i+1]

            return np.hstack((x, new_features.transpose(1, 0)))
        
        ex_values = df.loc[:, ['q_score', 'w_score', 'g_score', 'm_score', 'dow_score', 'consecutive holiday']].values
        ex_values = np.hstack((linear, ex_values))
        
        if(test):
            pass
        else:
            self.scaler = MinMaxScaler().fit(ex_values)
            self.poly = PolynomialFeatures(degree=2, include_bias=False).fit(ex_values)
        
        ex_values = self.scaler.transform(ex_values)
        return self.poly.transform(ex_values)
    
    
    def _get_object_data(self, df):
        return df['value'].values
    
    
    def _get_linear_x(self, df, diff=0):
        return np.array([i+diff for i in range(len(df))]).reshape(-1, 1)
    
    
    def _bias_train_optimization(self, linear, bias):
        linear_max = linear.max()
        linear_min = linear.min()
        ratio = (linear - linear_min) / (linear_max - linear_min)
        bias = bias * ratio
        return bias
    
    
    def _grid_search(self):
        best_accuracy = 0
        self.best_ratio = 0
        self.best_bias = 0
        
        for bias in np.arange(-200, 200, 5):
            for ratio in np.arange(0.0, 2.0, 0.05):
                pred_tmp = self.pred_linear + self.pred_bias * ratio + bias
                accuracy = calc_accuracy(self.y_train, pred_tmp)
                if(best_accuracy < accuracy):
                    best_accuracy = accuracy
                    self.best_ratio = ratio
                    self.best_bias = bias
    
    
    def fit(self, df):
        self.df_start_date = df.head(1)['date']
        
        X_linear_train = self._get_linear_x(df)
        y_train = self._get_object_data(df)
        self._fit_linear(X_linear_train, y_train)
        pred_linear = self.reg.predict(X_linear_train)
        
        X_train = self._get_explain_data(df, pred_linear.reshape(-1, 1))
        bias_tmp = y_train - pred_linear
        bias_train = self._bias_train_optimization(pred_linear, bias_tmp)
        self._fit_bias(X_train, bias_train)
        pred_bias = self._predict_bias(X_train)
    
        self.y_train = y_train
        self.pred_linear = pred_linear
        self.pred_bias = pred_bias
        
        if(self.gs):
            self._grid_search()
        
        if(self.view):
            visualize_graph(X_linear_train, self.y_train, self.pred_linear)
            visualize_graph(X_train, bias_train, self.pred_bias)
            
        return self
    
    
    def predict(self, df):
        Xl = self._get_linear_x(df, diff=(df['date'].head(1) - self.df_start_date).dt.days[0])
        pred_linear = self.reg.predict(Xl)
        X = self._get_explain_data(df, pred_linear.reshape(-1, 1), test=True)
        pred_bias = self._predict_bias(X, test=True)
        
        if(self.gs):
            pred = pred_linear + pred_bias * self.best_ratio + self.best_bias
        else:
            pred = pred_linear + pred_bias
            
        return pred
