
# coding: utf-8

import pandas as pd
import numpy as np

import pickle, os
from copy import deepcopy
from pprint import pprint


def to_datetime(obj):
    if(type(obj)==tuple):
        obj = pd.Timestamp(*obj)
    if(type(obj)==str):
        obj = pd.Timestamp(obj)
    
    return obj


def read_departure_data(CSV_PATH, country_string, country_id):
    df_all = pd.read_csv(CSV_PATH).fillna(0)
    
    if(country_id == -1):
        if(country_string == 'all'):
            df_tmp = df_all.loc[:, ['出発日', '台数判定の合計']]
        else:
            df_tmp = df_all.loc[df_all['国判定']==country_string, ['出発日', '台数判定の合計']]

    elif(contry_id != 0):
        script_dir = os.path.abspath(os.path.dirname(__file__))
        country_file = os.path.join(script_dir, 'pickle/country_code.pickle')
        with open(country_file, 'rb') as f:
            c_dict = pickle.load(f)
        country_code = c_dict[country_id]
        df_tmp = df_all.loc[df_all['国判定']==country_code, ['出発日', '台数判定の合計']]

    else: #(contry_id == 0)
        df_tmp = df_all.loc[:, ['出発日', '台数判定の合計']]

    df_tmp.columns = ['date', 'value']
    df_tmp['date'] = pd.to_datetime(df_tmp['date'])
    df_tmp['value'] = df_tmp['value'].apply(lambda x: int(str(x).replace(',', '')))
    dep_groups = df_tmp.groupby('date')

    df = pd.DataFrame(columns=['date', 'value'])
    for date, group in dep_groups:
        df_add = pd.DataFrame([[date, group['value'].sum()]], columns=['date', 'value'])
        df = df.append(df_add)

    return df.reset_index(drop=True)


def extract_by_time(df, START_DATE=(2013, 1, 1), END_DATE=None):
    START_DATE = to_datetime(START_DATE)

    extracted_df = df.loc[df['date'] >= START_DATE].reset_index(drop=True)
    if(END_DATE != None):
        END_DATE = to_datetime(END_DATE)
        extracted_df = extracted_df.loc[extracted_df['date'] <= END_DATE].reset_index(drop=True)

    return extracted_df


def create_dataframe(START_DATE, END_DATE):
    def daterange(s_date, e_date):
        for n in range(int((e_date - s_date).days)+1):
            yield s_date + pd.tseries.offsets.Day(n)

    START_DATE = to_datetime(START_DATE)
    END_DATE = to_datetime(END_DATE)

    df = pd.DataFrame()
    df['date'] = [date for date in daterange(START_DATE, END_DATE)]
    
    return df
        

def add_holiday_information(df):
    script_dir = os.path.abspath(os.path.dirname(__file__))
    holiday_file = os.path.join(script_dir, 'pickle/holiday.pickle')
    holiday = pd.read_pickle(holiday_file)

    df = pd.merge(df, holiday, on='date', how='left')
    df['public holiday'] = df['public holiday'].fillna(0)
    df.rename(columns={"public holiday" : "holiday"}, inplace=True)
    df.loc[df['date'].dt.weekday.isin([5, 6]), 'holiday'] = 1
    
    count = 0
    for idx in reversed(df.index):
        if(df.loc[idx, 'holiday'] == 1):
            count = count + 1
        else:
            count = 0
        df.loc[idx, 'consecutive holiday'] = count
    
    return df


class PeriodScore():
    def __init__(self):
        self.separate_days = [1, 9, 17, 25, 32]
    
    
    def fit(self, df):
        self.quarter_score = {}
        self.month_score = {}
        self.week_score = {}
        self.general_week_score = {i:0 for i in range(1, 5)}
        self.dow_score = {}
        self._calc_scores(df)
        return self
    
    
    def transform(self, df):
        for q_key, q_value in self.quarter_score.items():
            df.loc[df['date'].dt.quarter == q_key, 'q_score'] = q_value
        
        for m_key, m_value in self.month_score.items():
            cond_month = df['date'].dt.month==m_key
            df.loc[cond_month, 'm_score'] = m_value

            for week, (start_day, end_day) in enumerate(zip(self.separate_days[0:4], self.separate_days[1:5]), start=1):
                cond_min = df['date'].dt.day >= start_day
                cond_max = df['date'].dt.day < end_day
                cond_g = np.logical_and(cond_min, cond_max)
                cond_w = np.logical_and(cond_month, cond_g)

                w_dict_key = str(m_key)+'-'+str(week)
                df.loc[cond_w, 'w_score'] = self.week_score[w_dict_key]
                df.loc[cond_g, 'g_score'] = self.general_week_score[week]
        
        for dow_key, dow_value in self.dow_score.items():
            df.loc[df['date'].dt.weekday == dow_key, 'dow_score'] = dow_value
            
        return df
    
    
    def _calc_scores(self, df):
        self._calc_quarter_score(df)
        self._calc_month_and_week_score(df)
        self._calc_dow_score(df)
        
        
    def _calc_quarter_score(self, df):
        for quarter in range(1, 5):
            df_quarter = df.loc[df['date'].dt.quarter == quarter, ['value']]
            change_index = df_quarter.index
            q_score = float(df_quarter.mean())
            self.quarter_score[quarter] = q_score


    def _calc_month_and_week_score(self, df):
        for month in range(1, 13):
            df_month = df.loc[df['date'].dt.month == month, ['value', 'date']]
            m_index = df_month.index
            m_score = float(df_month['value'].mean())

            for i, (start_day, end_day) in enumerate(zip(self.separate_days[0:4], self.separate_days[1:5]), start=1):
                cond_min = df_month['date'].dt.day >= start_day
                cond_max = df_month['date'].dt.day < end_day
                cond = np.logical_and(cond_min, cond_max)

                df_week = df_month.loc[cond, ['value']]
                w_index = df_week.index
                w_score = (float(df_week['value'].mean()))

                self.general_week_score[i] = self.general_week_score[i] + w_score/12
                w_dict_key = str(month)+'-'+str(i)
                self.week_score[w_dict_key] = w_score

            self.month_score[month] = m_score
    
    
    def _calc_dow_score(self, df):
        for dow in range(7):
            df_dow = df.loc[df['date'].dt.weekday == dow, ['value']]
            dow_index = df_dow.index
            dow_score = float(df_dow.mean())
            self.dow_score[dow] = dow_score
    

    def check_scores(self):
        scores = [self.quarter_score, self.month_score, self.week_score, self.general_week_score, self.dow_score]
        s_names = ['quarter', 'month', 'week', 'general_week', 'day of week']
        
        for name, score in zip(s_names, scores):
            print('score of '+name)
            print('length : {}'.format(len(score)))
            pprint(score)
            print()

